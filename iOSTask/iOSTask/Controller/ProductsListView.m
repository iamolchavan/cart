//
//  ProductsListView.m
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import "ProductsListView.h"
#import "ImageHelper.h"
#import "ProductsList.h"
#import "Cart.h"
#import "ProductDetailsView.h"
#import "UIBarButtonItem+Badge.h"
#import "YSLTransitionAnimator.h"
#import "UIViewController+YSLTransition.h"

@interface ProductsListView ()<YSLTransitionAnimatorDataSource>
{
    ProductsList *productList;
    
    Product *selectedProduct;
    NSIndexPath *currentIndexPath;
}

@end

@implementation ProductsListView

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    productList = [ProductsList sharedInstance];
    
    [self configureNavigationBar];
    
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self ysl_removeTransitionDelegate];
}

- (void)viewDidAppear:(BOOL)animated
{
    Cart *cart = [Cart sharedInstance];
    
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%lu",(unsigned long)[cart.cartItems count]];
    
    float statusHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    float navigationHeight = self.navigationController.navigationBar.frame.size.height;
    
    [self ysl_addTransitionDelegate:self];
    [self ysl_pushTransitionAnimationWithToViewControllerImagePointY:statusHeight + navigationHeight
                                                   animationDuration:0.7];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)moveToProductDetails
{
    [self performSegueWithIdentifier:@"ProductDetailsView" sender:self];
}

- (void)configureNavigationBar
{
    UIImage *applicationLogoImage = [ImageHelper imageWithImage:[UIImage imageNamed:@"Cart"] scaleToSize:CGSizeMake(35, 35)];
    
    applicationLogoImage = [applicationLogoImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIButton *cartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cartButton.frame = CGRectMake(0, 0, 35, 35);
    [cartButton setImage:applicationLogoImage forState:UIControlStateNormal];
    [cartButton addTarget:self action:@selector(cartTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cartButton.layer.masksToBounds = YES;
    cartButton.layer.cornerRadius = 5.0f;
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:cartButton];

    self.navigationItem.rightBarButtonItem = rightBarButton;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
}

# pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [productList.products count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[productList.products objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellIdentifier = @"ImageAndTextTypeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:strCellIdentifier];
    }
    
    Product *product = [[productList.products objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;

    cell.imageView.image = [ImageHelper imageWithImage:[UIImage imageNamed:[product.imagePath firstObject]] scaleToSize:CGSizeMake(30, 30)];
    cell.textLabel.text = product.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"₹ %0.2f", product.price];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedProduct = [[productList.products objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    currentIndexPath = indexPath;
    [self moveToProductDetails];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [productList.productTypes objectAtIndex:section];
}

# pragma mark - Actions

- (IBAction)cartTapped:(id)sender
{
    [self performSegueWithIdentifier:@"CartView" sender:self];
}

# pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ProductDetailsView"])
    {
        ProductDetailsView *productDetailsView = (ProductDetailsView *)[segue destinationViewController];
        productDetailsView.selectedProduct = selectedProduct;
    }
}

#pragma mark -- YSLTransitionAnimatorDataSource
- (UIImageView *)pushTransitionImageView
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:currentIndexPath];
    Product *product = [[productList.products objectAtIndex:currentIndexPath.section] objectAtIndex:currentIndexPath.row];
    cell.imageView.image = [UIImage imageNamed:[product.imagePath firstObject]];
    return cell.imageView;
    
}

- (UIImageView *)popTransitionImageView
{
    return nil;
}

@end
