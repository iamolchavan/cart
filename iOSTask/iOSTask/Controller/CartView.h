//
//  CartView.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartView : UIViewController <UITableViewDataSource, UITableViewDelegate,UIViewControllerTransitioningDelegate>



@property (strong, nonatomic) IBOutlet UITableView *tblCartItems;
@property (strong, nonatomic) IBOutlet UILabel *totalItemsLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnPurchase;

- (IBAction)btnPurchaseTapped:(id)sender;
@end
