//
//  ProductDetailsView.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface ProductDetailsView : UIViewController

@property Product *selectedProduct;

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;

@property (strong, nonatomic) IBOutlet UILabel *productNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *productPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *productQuantityLabel;

@property (strong, nonatomic) IBOutlet UIStepper *quantityStepper;
@property (strong, nonatomic) IBOutlet UIButton *btnAddToCart;

- (IBAction)btnAddToCartTapped:(id)sender;

- (IBAction)productQuantityChanged:(id)sender;

@end
