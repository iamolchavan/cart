//
//  CartView.m
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import "CartView.h"

#import "ImageHelper.h"
#import "Cart.h"

#import "ProductDetailsView.h"

#import "CustomModalViewController.h"
#import "PresentingAnimationController.h"
#import "DismissingAnimationController.h"
#import <pop/POP.h>

@interface CartView ()<CustomModalViewDelegate>
{
    Cart *cart;
    
    Product *selectedItem;
}
@end

@implementation CartView

@synthesize tblCartItems, totalItemsLabel, totalAmountLabel, btnPurchase;

# pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Cart";
    
    cart = [Cart sharedInstance];
//    [cart getCartInfo];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setCartInfo];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

# pragma mark - Private Methods

- (void)showPurchaseCompleteMessage
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success"
                                                                   message:@"Your order is placed."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action){
                                                         dispatch_async(dispatch_get_main_queue(), ^{
                                                             [cart resetCartItems];
                                                             [self.navigationController popToRootViewControllerAnimated:YES];
                                                         });
                                                     }];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)setCartInfo
{
    if ([cart.cartItems count] == 0)
    {
        btnPurchase.enabled = false;
    }
    
    totalItemsLabel.text = [NSString stringWithFormat:@"Total Items : %i", [cart getTotalItems]];
    totalAmountLabel.text = [NSString stringWithFormat:@"Total Amount : ₹ %0.2f", [cart getTotalAmount]];
    
    [tblCartItems reloadData];
    
    if ([cart.cartItems count] > 0)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                                               target:self
                                                                                               action:@selector(resetCartTapped:)];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
}

- (void)moveToProductDetails
{
    [self performSegueWithIdentifier:@"ProductDetailsView" sender:self];
}

# pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cart.cartItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *strCellIdentifier = @"ImageAndTextTypeCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCellIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:strCellIdentifier];
    }
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    Product *cartItem = [cart.cartItems objectAtIndex:indexPath.row];
    
    cell.imageView.image = [ImageHelper imageWithImage:[UIImage imageNamed:[cartItem.imagePath firstObject]] scaleToSize:CGSizeMake(30, 30)];
    cell.textLabel.text = cartItem.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"₹ %0.2f X %i items\nTotal ₹ %0.2f", cartItem.price, cartItem.quantities, (cartItem.price * cartItem.quantities)];
    cell.detailTextLabel.numberOfLines = 0;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        selectedItem = [cart.cartItems objectAtIndex:indexPath.row];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure?"
                                                                       message:@"Remove item from cart"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action){
                                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                                  [cart removeCartItem:selectedItem];
                                                                  
                                                                  NSArray *cells = [NSArray arrayWithObjects:
                                                                                    [NSIndexPath indexPathForRow:indexPath.row inSection:0],
                                                                                    nil];
                                                                  [tblCartItems beginUpdates];
                                                                  [tblCartItems deleteRowsAtIndexPaths:cells withRowAnimation:UITableViewRowAnimationFade];
                                                                  [tblCartItems endUpdates];
                                                                  
                                                                  [self setCartInfo];
                                                              });
                                                          }];
        
        [alert addAction:yesAction];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action){
                                                             }];
        
        [alert addAction:cancelAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedItem = [cart.cartItems objectAtIndex:indexPath.row];
    
    [self moveToProductDetails];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([cart.cartItems count] == 0)
    {
        return @"No Items in cart";
    }
    return @"Cart Items";
}

# pragma mark - Actions

- (IBAction)btnPurchaseTapped:(id)sender
{
    
    CustomModalViewController *modalVC = [self.storyboard instantiateViewControllerWithIdentifier:@"customModal"];
    modalVC.delegateCustomModal = self;
    
    modalVC.transitioningDelegate = self;
    
    modalVC.modalPresentationStyle = UIModalPresentationCustom;
    
    [self.navigationController presentViewController:modalVC animated:YES completion:nil];
}

#pragma mark - UIViewControllerTransitionDelegate -

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    return [[PresentingAnimationController alloc] init];
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return [[DismissingAnimationController alloc] init];
}


- (IBAction)resetCartTapped:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure?"
                                                                   message:@"Remove all items in cart"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action){
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [cart resetCartItems];
                                                              [self.navigationController popToRootViewControllerAnimated:YES];
                                                          });
                                                      }];
    
    [alert addAction:yesAction];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action){
                                                         }];
    
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

# pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ProductDetailsView"])
    {
        ProductDetailsView *productDetailsView = (ProductDetailsView *)[segue destinationViewController];
        productDetailsView.selectedProduct = selectedItem;
    }
}

-(void)didSubmit{
    NSLog(@"YES");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showPurchaseCompleteMessage];
    });
}



@end