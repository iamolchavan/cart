//
//  ProductType.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#ifndef ProductType_h
#define ProductType_h

typedef NS_ENUM(NSInteger, ProductType)
{
    ELECTRONICS = 1,
    FURNITURE,
    OTHER,
};


#endif /* ProductType_h */
