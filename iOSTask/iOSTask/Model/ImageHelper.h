//
//  ImageHelper.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageHelper : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image scaleToSize:(CGSize)newSize;

@end
