//
//  ProductsList.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Product.h"

@interface ProductsList : NSObject

@property NSMutableArray *products;

@property NSMutableArray *productTypes;

+ (instancetype)sharedInstance;

- (ProductType)getProductTypeEquivalentForString:(NSString *)strProductType;

@end

