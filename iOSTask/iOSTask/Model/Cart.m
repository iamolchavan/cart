//
//  Cart.m
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import "Cart.h"
#import "ProductsList.h"

NSString *kCartInfo = @"CartInfo";

@interface Cart ()
{
    ProductsList *productsList;
}
@end

@implementation Cart

@synthesize cartItems;

static Cart *cart = nil;

+ (instancetype)sharedInstance
{
    @synchronized(self)
    {
        if (cart == nil)
        {
            cart = [Cart new];
            cart.cartItems = [NSMutableArray new];
            [cart getCartInfo];

        }
    }
    return cart;
}

/**
 * @brief Save all products in cart to persist after relaunch of application (Product Id and quantity)
 */
- (void)saveCartInfo
{
    
    NSMutableDictionary *productsInfo = [NSMutableDictionary new];
    
    for (Product *product in cart.cartItems)
    {
        NSDictionary *info = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                                  product.name,
                                                                  [NSNumber numberWithFloat:product.price],
                                                                  product.imagePath,
                                                                  [NSNumber numberWithInt:(int)product.productType],[NSNumber numberWithInt:product.quantities], nil]
                                                         forKeys:[NSArray arrayWithObjects:
                                                                  @"name",
                                                                  @"price",
                                                                  @"imagePath",
                                                                  @"productType",@"quantities", nil]];
        
        
        [productsInfo setValue:info forKey:product.Id];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *productsData = [NSKeyedArchiver archivedDataWithRootObject:productsInfo];
    [userDefaults setObject:productsData forKey:kCartInfo];
    
    [userDefaults synchronize];
    
    
    
}

/**
 * @brief Get all products in cart which saved on last use of application
 */
- (void)getCartInfo
{

    [cartItems removeAllObjects];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *productsData = [userDefaults objectForKey:kCartInfo];
    
    if (productsData == nil)
        return;
    
    NSMutableDictionary *productsInfo = [NSKeyedUnarchiver unarchiveObjectWithData:productsData];
    
    NSArray *productIdArray = [productsInfo allKeys];
    
    if ([productIdArray count] > 0)
    {
        for (NSString *strProductId in productIdArray)
        {
            NSDictionary *info = [productsInfo valueForKey:strProductId];
            
            Product *product = [Product new];
            product.Id = strProductId;
            product.name = [info valueForKey:@"name"];
            product.price = [[info valueForKey:@"price"] floatValue];
            product.imagePath = [info valueForKey:@"imagePath"];
            product.productType = [[info valueForKey:@"productType"] intValue];
            product.quantities = [[info valueForKey:@"quantities"] intValue];
            
            [cartItems addObject:product];
        }
    }
    
}

/**
 * @brief Add product to cart
 */
- (void)addCartItem:(Product *)cartItem
{
    if ([cartItems containsObject:cartItem])
    {
        [cartItems removeObject:cartItem];
    }
    [cartItems addObject:cartItem];
    
    [self saveCartInfo];
}

/**
 * @brief Removes product from cart and resets quantity count to 1
 */
- (void)removeCartItem:(Product *)cartItem
{
    cartItem.quantities = 1;
    
    if ([cartItems containsObject:cartItem])
    {
        [cartItems removeObject:cartItem];
    }
    
    [self saveCartInfo];
}

/**
 * @brief Clears cart
 */
- (void)resetCartItems
{
    for (Product *product in cartItems)
    {
        product.quantities = 1;
    }
    [cartItems removeAllObjects];
    
    [self saveCartInfo];
}

/**
 * @brief Get total amount to be paid by user
 */
- (float)getTotalAmount
{
    float amount = 0;
    
    for (Product *product in cartItems)
    {
        amount += product.price * product.quantities;
    }
    
    return amount;
}

/**
 * @brief Gets total items added in cart
 */
- (int)getTotalItems
{
    int items = 0;
    
    for (Product *product in cartItems)
    {
        items += product.quantities;
    }
    
    return items;
}

@end