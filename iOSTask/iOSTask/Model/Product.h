//
//  Product.h
//  iOSTask
//
//  Created by Yuyutsu on 28/05/16.
//  Copyright © 2016 iAmolChavan. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ProductType.h"

@interface Product : NSObject

@property NSString *Id;

@property NSArray *imagePath;
@property NSString *name;
@property float price;

@property ProductType productType;
@property int quantities;

@end