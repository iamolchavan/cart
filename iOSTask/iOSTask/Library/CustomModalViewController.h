//
//  CustomModalViewController.h
//  POPDemo
//
//  Created by Simon Ng on 22/12/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomModalViewDelegate <NSObject>

-(void)didSubmit;

@end
@interface CustomModalViewController : UIViewController
@property(nonatomic, weak) id<CustomModalViewDelegate> delegateCustomModal;
@end
